// Future improvements: have the Pulser "bounce" up and down along the length of the Tracer
// Improve the "gravitate" behavior of the Tracers to make their interactions more interesting

// A Tracer encapsulates a Pulser (the "head" of the Tracer) and a tail that follows the Pulser as
// it moves.
class Tracer {
  PVector head;
  Pulser pulse;
  float head_angle;
  PVector head_direction;
  PVector tail_direction;
  float speed;
  ArrayList<PVector> turn_points;
  ArrayList<Tracer> seen;
  boolean dead; // True if the tracer goes off the screen completely.
  
  // Constants
  final int LINE_LENGTH = 9000;
  final int BORDER_FALLOFF = 100;
  final float GRAVITATION_DISTANCE = 40;
  
  Tracer() {
    reset();
  }
  
  void reset() {

    speed = random(2,4);
    pulse = new Pulser(speed * 0.25);
    int direction = (int) random(0,4);
    float start_x = random(0, width);
    float start_y = random(0, height);
    if (direction == 0) { // west
      head = new PVector(0, start_y);
      head_angle = radians(0);
    } else if (direction == 1) { // east
      head = new PVector(width, start_y);
      head_angle = radians(180);
    } else if (direction == 2) { // south
      head = new PVector(start_x, 0);
      head_angle = radians(90);
    } else if (direction == 3) { // north
      head = new PVector(start_x, height);
      head_angle = radians(270);
    }
    head_direction = new PVector(cos(head_angle), sin(head_angle));
    tail_direction = new PVector(head_direction.x, head_direction.y);
    turn_points = new ArrayList<PVector>();
    seen = new ArrayList<Tracer>();
  }
  
  void draw() {
    if (!dead) {
      head.x += head_direction.x * speed;
      head.y += head_direction.y * speed;
      pulse.move(head.x, head.y);
      pulse.draw();
    }
    
    stroke(219, 120, 40);
    
    if (turn_points.isEmpty()) {
      tail_direction.x = -head_direction.x;
      tail_direction.y = -head_direction.y;
      drawTailFrom(head.x, head.y, LINE_LENGTH);
    } else {
      int i = turn_points.size() - 1;
      float length_used = dist(head.x, head.y, turn_points.get(i).x, turn_points.get(i).y);
      if (length_used >= LINE_LENGTH) {
        turn_points.clear();
        tail_direction.x = -head_direction.x;
        tail_direction.y = -head_direction.y;
        drawTailFrom(head.x, head.y, LINE_LENGTH);
        return;
      }
      line(head.x, head.y, turn_points.get(i).x, turn_points.get(i).y);
      
      while (true) {
        if (i == 0) {
          drawTailFrom(turn_points.get(0).x, turn_points.get(0).y, LINE_LENGTH - length_used);
          break;
        }
        float distance = dist(turn_points.get(i).x, turn_points.get(i).y, turn_points.get(i-1).x, turn_points.get(i-1).y);
        if (distance + length_used > LINE_LENGTH) {
          tail_direction.x = turn_points.get(i-1).x - turn_points.get(i).x;
          tail_direction.y = turn_points.get(i-1).y - turn_points.get(i).y;
          tail_direction.normalize();
          drawTailFrom(turn_points.get(i).x, turn_points.get(i).y, LINE_LENGTH - length_used);
          for (int j = 0; j < i; j++) {
            turn_points.remove(0);
          }
          break;
        } else {
          //   draw a line from the current node to the next one 
          line(turn_points.get(i).x, turn_points.get(i).y, turn_points.get(i-1).x, turn_points.get(i-1).y);
          length_used += distance;
        }
        i--;
      }
    }
  }
  
  private void drawTailFrom(float x, float y, float len) {
    PVector tail = new PVector(x, y);
    tail.x += tail_direction.x * len;
    tail.y += tail_direction.y * len;
    line(x, y, tail.x, tail.y);
  }
  
  void gravitate(Tracer other) {
    // TODO: add a check so that if head is out of bounds it doesn't gravitate any further
    if (!seen.contains(other)) {
      turn_points.add(new PVector(head.x, head.y));
      // now turn 90 degrees
      head_angle += radians(90);
      head_direction.x = cos(head_angle);
      head_direction.y = sin(head_angle);
      seen.add(other);
    }
  }
  
  boolean dead() {
    // TODO: rewrite this so that it checks if the tail is off the edge instead
    if ((head.x + BORDER_FALLOFF) < 0 || (head.x - BORDER_FALLOFF) > width || (head.y + BORDER_FALLOFF) < 0 || (head.y - BORDER_FALLOFF) > height) {
      return true;
    } else {
      return false;
    }
  }
  
  boolean near(Tracer other) {
    return dist(head.x, head.y, other.head.x, other.head.y) < GRAVITATION_DISTANCE;
  }
}

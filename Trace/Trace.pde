import ddf.minim.*;
import ddf.minim.signals.*;
import ddf.minim.analysis.*;
import ddf.minim.effects.*;

ArrayList<Tracer> tracers = new ArrayList<Tracer>();
Terminal term = new Terminal();
int bg = 0;
final int tracer_count = 20;
Minim minim;
AudioPlayer player;

void setup() {
  size(640, 480);
  background(128,128,128);
  smooth();
  frameRate(30);
  for (int i = 0; i < tracer_count; i++) {
    tracers.add(new Tracer());
  }
  minim = new Minim(this);
  player = minim.loadFile("song.mp3");
  player.play();
}

void draw() {
  background(bg);
  int addcount = 0;
  for (Tracer t : tracers) {
    t.draw();
    if (t.dead() && t.dead == false) {
      t.dead = true;
      t.pulse = null;
      addcount++;
    }
    for (Tracer t2 : tracers) {
      if (t != t2 && t.near(t2)) {
        t.gravitate(t2);
      }
    }
  }
  for (int i = 0; i < addcount; i++) {
    tracers.add(new Tracer());
  }
  noFill();
}

void stop() {
  player.close();
  minim.stop();
  super.stop();
}

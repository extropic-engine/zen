
// A pulsating box object.
class Pulser {
  float x;
  float y;
  ArrayList<Pulse> pulses;
  
  final int PULSE_MAX_WIDTH = 25;
  
  Pulser(float pulse_speed) {
    pulses = new ArrayList<Pulse>();
    x = 0;
    y = 0;
    ColorBreather cb = new ColorBreather();
    cb.orange();
    for (int i = 0; i < PULSE_MAX_WIDTH; i++) {
      cb.draw();
      pulses.add(new Pulse(i, cb.currentColor, pulse_speed));
    }
    cb.draw();
    pulses.add(new Pulse(PULSE_MAX_WIDTH, cb.currentColor, 0));
  }
  
  void move(float x, float y) {
    this.x = x;
    this.y = y;
  }
  
  void draw() {
    for (int i = 0; i < pulses.size(); i++) {
      Pulse p = pulses.get(i);
      stroke(p.c);
      rect(x - (p.w / 2), y - (p.w / 2), p.w, p.w);
      noFill();
      p.w += p.speed;
      if (p.w > PULSE_MAX_WIDTH) {
        p.w = 0;
        pulses.remove(i);
        pulses.add(p);
      }
    }
  }
}

class Pulse {
  float w; // width of the square
  color c; // color of the square
  float speed; // pulsation speed
  
  Pulse(float w, color c, float speed) {
    this.w = w;
    this.c = c;
    this.speed = speed;
  }
}

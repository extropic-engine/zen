static class Settings {
  static int num_particles = 10;
  static float G = 0.05;
  static float M = 2;
  static boolean use_lines = true;
  static float color_r = 255;
  static float color_g = 0;
  static float color_b = 0;
  static boolean random_colors = false;
  static boolean cluster_particles = false;
  static int sim_speed = 1;
  static boolean load_particles;
  static boolean wall_bouncing = false;
  static boolean angular_momentum = true;
  static ArrayList<PVector> particles;
}

class SettingsParser {
  
  boolean load(String parameter_file) {
    BufferedReader reader = createReader(parameter_file);
    if (reader == null) {
      defaults();
      return false;
    }
    try {
      Settings.num_particles = readInt(reader);
      Settings.G = readFloat(reader);
      Settings.M = readFloat(reader);
      Settings.use_lines = readBoolean(reader);
      Settings.color_r = readFloat(reader);
      Settings.color_g = readFloat(reader);
      Settings.color_b = readFloat(reader);
      Settings.random_colors = readBoolean(reader);
      Settings.cluster_particles = readBoolean(reader);
      Settings.sim_speed = readInt(reader);
      Settings.load_particles = readBoolean(reader);
      if (Settings.load_particles) {
        Settings.particles = new ArrayList<PVector>();
        for (int i = 0; i < Settings.num_particles; i++) {
          Settings.particles.add(readPVector(reader));
        }
      }
    } catch (IOException e) {
      defaults();
      return false;
    }
    return true;
  }
  
  PVector readPVector(BufferedReader reader) throws IOException {
    String file_line = readFileLine(reader);
    String[] values = file_line.split(",");
    if (values.length < 2) {
      throw new IOException();
    }
    return new PVector(int(values[0]), int(values[1]));
  }
  
  boolean readBoolean(BufferedReader reader) throws IOException {
    return boolean(readFileLine(reader));
  }
  
  float readFloat(BufferedReader reader) throws IOException {
    return float(readFileLine(reader));
  }
  
  int readInt(BufferedReader reader) throws IOException {
    return int(readFileLine(reader));
  }
  
  String readFileLine(BufferedReader reader) throws IOException {
    while (true) {
      String file_line = reader.readLine();
      if (file_line == null) {
        throw new IOException();
      }
      file_line = file_line.split("#")[0].trim();
      if (!file_line.equals("")) {
        return file_line;
      }
    }
  }
  
  void defaults() {
    Settings.num_particles = 100;
    Settings.G = 0.05;
    Settings.M = 2;
    Settings.use_lines = true;
    Settings.color_r = 255;
    Settings.color_g = 0;
    Settings.color_b = 0;
    Settings.random_colors = false;
    Settings.cluster_particles = false;
    Settings.sim_speed = 1;
  }
  
}

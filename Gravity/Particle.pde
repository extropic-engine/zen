// future improvements: make particles bounce off objects at correct angles
// fix slingshot singularity bug

class Particle {
  PVector location;
  PVector velocity;
  color c;
  float speed;
  float mass;
  
  Particle() {
    if (Settings.random_colors) {
      c = color(random(0,255), random(0,255), random(0, 255));
    } else {
      c = color(Settings.color_r, Settings.color_g, Settings.color_b);
    }
    if (Settings.cluster_particles) {
      location = new PVector(random((width/2) - 25, (width/2) + 25), random((height/2) - 25, (height/2) + 25));
    } else {
      location = new PVector(random(0,width), random(0, height));
    }
    if (Settings.angular_momentum) {
      float d = dist(location.x, location.y, width/2, height/2);
      //velocity = new PVector(cos((location.x / (width)) - 1), 0);//cos((location.y / (height/2)) - 1));
      velocity = new PVector(0,0);
      velocity.mult(d * 0.001);
    } else {
      velocity = new PVector(0, 0);
    }
    mass = Settings.M;
  }
  
  void draw() {
    stroke(c);
    
    line(location.x, location.y, location.x + velocity.x, location.y + velocity.y);
    location.x += velocity.x;
    location.y += velocity.y;
    
    // wall bouncing code
    // use this for sharp angles
    if (Settings.wall_bouncing) {
      if (location.x < 0 || location.x > width) {
        velocity.x = -velocity.x;
      }
      if (location.y < 0 || location.y > height) {
        velocity.y = -velocity.y;
      }
    }
    
  }
  
  void gravitate(Particle p) {
    if (p != this) {
      float d = dist(p.location.x, p.location.y, this.location.x, this.location.y);
      float denom = pow(d, 2);
      if (d > 4) {
        float effect = Settings.G * ((this.mass * p.mass) / denom);
        PVector direction = new PVector(p.location.x - location.x, p.location.y - location.y);
        direction.normalize();
        direction.mult(effect);
        velocity.x += direction.x;
        velocity.y += direction.y;
      }
    }
  }
  
  void gravitate(ArrayList<Particle> particles) {
    for (Particle p : particles) {
      gravitate(p);
    }
  }
}

// Future improvements: random colors within a range (shades of blue)
// Parameter: add "speed of light" speed limit
// Parameter: background color

ArrayList<Particle> particles;
Singularity blackHole = null;
SettingsParser parser = new SettingsParser();
String p_file;
PFont font;
final float version = 0.2;
boolean directions_printed = false;
boolean loaded_ok = false;
boolean filename_printed = false;
boolean show_directions = false;
final String directions = "left click to create particles\n" +
                          "hold right click to create a singularity\n" +
                          "press 0-9 to switch between parameter files\n" +
                          "press spacebar to choose a file\n" +
                          "press 's' to create a permanent singularity\n" +
                          "press 't' to toggle lines\n";

void setup() {
  //size(750, 750);
  //size(screen.height, screen.height);
  size(displayWidth, displayHeight);
  smooth();
  frameRate(30);
  font = createFont("Verdana", 14);
  textFont(font);
  reset("parameters.txt");
}

void reset(String parameter_file) {
  background(0);
  p_file = parameter_file;
  if (parser.load(parameter_file)) {
    loaded_ok = true;
  } else {
    loaded_ok = false;
  }
  particles = new ArrayList<Particle>();
  for(int i = 0; i < Settings.num_particles; i++) {
    particles.add(new Particle());
    if (Settings.load_particles) {
      particles.get(i).location.x = Settings.particles.get(i).x;
      particles.get(i).location.y = Settings.particles.get(i).y;
    }
  }
  Settings.particles = null;
  directions_printed = false;
  filename_printed = false;
}

void draw() {
  if (!Settings.use_lines) {
    background(0);
    fill(255);
    text("framerate: " + frameRate, width - 150, 30);
    noFill();
    if (blackHole != null) { blackHole.draw(); }
  }
  fill(255);
  printDirections();
  noFill();
  for (int i = 0; i < Settings.sim_speed; i++) {
    for (Particle p : particles) {  
      p.gravitate(particles);
      if (blackHole != null) { p.gravitate(blackHole); }
    }
    for (Particle p : particles) {
      p.draw();
    }
    
  }
}

void mousePressed() {
  if (mouseButton == RIGHT) {
    blackHole = new Singularity();
    blackHole.location.x = mouseX;
    blackHole.location.y = mouseY;
    blackHole.mass = 10;
  }
  if (mouseButton == LEFT) {
    Particle p = new Particle();
    p.location.x = mouseX;
    p.location.y = mouseY;
    particles.add(p);
  }
}

void mouseDragged() {
  if (mouseButton == RIGHT) {
    blackHole.location.x = mouseX;
    blackHole.location.y = mouseY;
  }
  if (mouseButton == LEFT) {
    Particle p = new Particle();
    p.location.x = mouseX;
    p.location.y = mouseY;
    particles.add(p);
  }
}

void mouseReleased() {
  if (mouseButton == RIGHT) {
    blackHole = null;
  }
}

void printDirections() {
  if (Settings.use_lines && directions_printed) {
    return;
  }
  
  if (!Settings.use_lines || !filename_printed) {
    if (!loaded_ok) {
      text("gravity " + version + "\ncould not load "+p_file, 30, 30);
    } else {
      text("gravity " + version + "\nusing "+p_file, 30, 30);
    }
  }
  
  if (show_directions) {
    if (Settings.use_lines) {
      text("\n\npress 'k' to show directions\n"+directions, 30, 30);
      directions_printed = true;
    } else {
      text("\n\npress 'k' to hide directions\n"+directions, 30, 30);
    }
  } else if (!Settings.use_lines || !filename_printed) {
    text("\n\npress 'k' to show directions", 30, 30);
  }
  filename_printed = true;
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      Settings.sim_speed++;
    } else if (keyCode == DOWN) {
      if (Settings.sim_speed > 1) {
        Settings.sim_speed--;
      }
    }
  } else if (key == 't') {
    Settings.use_lines = !Settings.use_lines;
    directions_printed = false;
    filename_printed = false;
    background(0);
  } else if (key == 's') {
    Singularity s = new Singularity();
    s.location.x = mouseX;
    s.location.y = mouseY;
    particles.add(s);
  } else if (key == 'k') {
    if (Settings.use_lines) {
      show_directions = true;
    } else {
      show_directions = !show_directions;
    }
  } else if (key == '1') {
    reset("parameters.txt");
  } else if (key == '2') {
    reset("parameters2.txt");
  } else if (key == '3') {
    reset("parameters3.txt");
  } else if (key == '4') {
    reset("parameters4.txt");
  } else if (key == '5') {
    reset("parameters5.txt");
  } else if (key == '6') {
    reset("parameters6.txt");
  } else if (key == '7') {
    reset("parameters7.txt");
  } else if (key == '8') {
    reset("parameters8.txt");
  } else if (key == '9') {
    reset("parameters9.txt");
  } else if (key == '0') {
    reset("parameters10.txt");
  } else if (key == ' ') {
    selectInput("choose a parameter file", "fileSelected");
  }
}

void fileSelected(File f) {
  if (f != null) {
    reset(f.getAbsolutePath());
  }
}

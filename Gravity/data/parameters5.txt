# Default parameter set
# Blank lines and anything after a '#' is ignored.

500    # Number of particles
0.05   # Gravitational constant
5.0    # Mass of particles
false # Whether to draw trails behind the particles
0      # Color of particles (R)
255    # Color of particles (G)
0      # Color of particles (B)
false  # Whether to use randomized colors
false  # Whether to cluster particles or spread them randomly
2      # Simulation speed (integer, 1 is normal speed)
false  # Whether to randomize particles. If this is false, you need to list the particles afterwards
class Singularity extends Particle {
  static final float STREAK_LENGTH = 0.5;
  static final int STREAK_COUNT = 10;
  static final float DIAM = 30;
  ArrayList<PVector> streaks;
  
  Singularity() {
    super();
    streaks = new ArrayList<PVector>();
    for (int i = 0; i < STREAK_COUNT; i++) {
      streaks.add(new PVector(random(-DIAM, DIAM), random(-DIAM, DIAM)));
    }
  }
  
  void draw() {
    if (!Settings.use_lines) {
      stroke(c);
      //fill(0);
      ellipse(location.x, location.y, DIAM, DIAM);
      noFill();
      for (PVector streak : streaks) {
        line(location.x + streak.x, location.y + streak.y, location.x + (streak.x * STREAK_LENGTH), location.y + (streak.y * STREAK_LENGTH));
        streak.mult(0.9);
      }
      int addcount = 0;
      for (int i = streaks.size() - 1; i >= 0; i--) {
        if (streaks.get(i).x < 1 && streaks.get(i).y < 1) {
          streaks.remove(i);
          addcount++;
        }
      }
      for (int i = 0; i < addcount; i++) {
        streaks.add(new PVector(random(-DIAM, DIAM), random(-DIAM, DIAM)));
      }
    } else {
      stroke(c);
      ellipse(location.x, location.y, 30, 30);
    }
  }
}

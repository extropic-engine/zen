static class HallRaiser {
  static void raiseHall(float x, float y, FWorld world) {
    FBox hall = new FBox(100,10);
    hall.setStatic(true);
    hall.setPosition(x, y);
    FBox hall2 = new FBox(10,100);
    hall2.setStatic(true);
    hall2.setPosition(x, y);
    world.add(hall);
    world.add(hall2);
  }
}

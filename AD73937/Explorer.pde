class Explorer extends FPoly {
  final float RADIUS = 10.0;
  final float DENSITY = 1;
  final float DAMPING = 8;
  final float ANGULAR_DAMPING = 10;
  final float FORCE = 800;
  final float ANGULAR_FORCE = 2;
  
  Explorer(float x, float y) {
    super();
    this.setPosition(x, y);
    
    // calculate points of the triangle
    PVector t1 = PVector.mult(PVector.fromAngle(0), RADIUS);
    PVector t2 = PVector.mult(PVector.fromAngle(((2.0/3.0) * PI)), RADIUS);
    PVector t3 = PVector.mult(PVector.fromAngle(-((2.0/3.0) * PI)), RADIUS);
    this.vertex(t1.x, t1.y);
    this.vertex(t2.x, t2.y);
    this.vertex(0, 0);
    this.vertex(t3.x, t3.y);
    
    // set movement constants
    this.setDensity(DENSITY);
    this.setDamping(DAMPING);
    this.setAngularDamping(ANGULAR_DAMPING);
    this.setGroupIndex(-1);
  }
  
  void forward() {
    PVector orientation = PVector.fromAngle(this.getRotation());
    this.addForce(orientation.x * FORCE, orientation.y * FORCE);
  }
  
  void rotateLeft() {
    this.addTorque(-ANGULAR_FORCE);
  }
  
  void rotateRight() {
    this.addTorque(ANGULAR_FORCE);
  }
}

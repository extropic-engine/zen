class BetterInput {
  
  final int INVALID = 0;
  final int FORWARD = 1;
  final int BACKWARD = 2;
  final int CLOCKWISE = 3;
  final int COUNTERCLOCKWISE = 4;
  final int SPACE = 5;
  
  HashMap<Integer, Boolean> keysDown = new HashMap<Integer, Boolean>();
  
  BetterInput() {
  }
  
  boolean isDown(Integer k) {
    Boolean result = keysDown.get(k);
    if (result == null) {
      return false;
    }
    return result;
  }
  
  void keyPressed() {
    keysDown.put(map(), true);
  }
  
  void keyReleased() {
    keysDown.put(map(), false);
  }
  
  int map() {
    if (key == CODED) {
      if (keyCode == UP) {
        return FORWARD;
      } else if (keyCode == DOWN) {
        return BACKWARD;
      } else if (keyCode == LEFT) {
        return COUNTERCLOCKWISE;
      } else if (keyCode == RIGHT) {
        return CLOCKWISE;
      }
    } else {
      if (key == ' ') {
        return this.SPACE;
      }
    }
    return this.INVALID;
  }
}

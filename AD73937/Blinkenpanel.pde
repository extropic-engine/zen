class Blinkenpanel {
  final int BLINKENS_ACROSS = 5;
  final int BLINKENS_DOWN = 20;
  final int BLINK_RATE = 10; // higher is slower
  
  PVector position = new PVector();
  PVector dimensions = new PVector();
  
  int blink_frame = 0;
  
  boolean[][] blinkens = new boolean[BLINKENS_ACROSS][BLINKENS_DOWN];
  
  Blinkenpanel(float x, float y, float w, float h) {
    position.set(x, y, 0);
    dimensions.set(w, h, 0);
  }
  
  void draw() {
    noStroke();
    
    float blinkenWidth = dimensions.x / (float) BLINKENS_ACROSS;
    float blinkenHeight = dimensions.y / (float) BLINKENS_DOWN;
    for (int i = 0; i < BLINKENS_ACROSS; i++) {
      for (int m = 0; m < BLINKENS_DOWN; m++) {
        if (blinkens[i][m]) {
          fill(random(255), random(255), random(255));
          rect(position.x + (i * blinkenWidth), position.y + (m * blinkenHeight), (i+1) * blinkenWidth, (m+1) * blinkenHeight);
        }
        if (blink_frame == BLINK_RATE) {
          blinkens[i][m] = random(1) < 0.5;
        }
      }
    }
    blink_frame++;
    if (blink_frame > BLINK_RATE) {
      blink_frame = 0;
    }
  }
}

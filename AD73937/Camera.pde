class Camera extends FCircle {
  final float DENSITY = 1;
  final float DAMPING = 20;
  final float ANGULAR_DAMPING = 10;
  final float FORCE = 1000;
  final float ANGULAR_FORCE = 2;

  PVector destination = new PVector();
  
  Camera(float x, float y) {
    super(10);
    this.setPosition(x, y);
    
    // set movement constants
    this.setDensity(DENSITY);
    this.setDamping(DAMPING);
    this.setAngularDamping(ANGULAR_DAMPING);
    
    this.setDrawable(false);
    this.setGroupIndex(-1);
  }
  
  void setDestination(float x, float y) {
    destination.set(x, y, 0);
  }
  
  void update() {
    PVector force = PVector.sub(destination, new PVector(this.getX(), this.getY()));
    force.mult(FORCE);
    this.setForce(force.x, force.y);
  }
}

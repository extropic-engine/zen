import fisica.util.nonconvex.*;
import fisica.*;

FWorld world;
Explorer you;
BetterInput input = new BetterInput();
Camera cam;
Blinkenpanel blink;

void setup() {
  size(displayWidth, displayHeight);
  smooth();
  frameRate(30);
  
  blink = new Blinkenpanel(displayWidth - (displayWidth / 10.0), 0, displayWidth / 10.0, displayHeight);
  
  // Fisica dependent setup below this line!
  Fisica.init(this);
  world = new FWorld();
  world.setGravity(0,0);
  
  you = new Explorer(displayWidth/2.0, displayHeight/2.0);
  you.setFill(64);
  world.add(you);
  
  FBox box = new FBox(40, 20);
  box.setPosition(displayWidth/3.0, displayHeight/2.0);
  world.add(box);
  // add a test object for reference
  cam = new Camera(displayWidth/2.0, displayHeight/2.0);
  world.add(cam);
  HallRaiser.raiseHall(displayWidth/4.0, displayHeight/4.0, world);
}

void draw() {
  if (input.isDown(input.COUNTERCLOCKWISE)) {
    you.rotateLeft();
  }
  if (input.isDown(input.CLOCKWISE)) {
    you.rotateRight();
  }
  if (input.isDown(input.FORWARD)) {
    you.forward();
  }
  if (input.isDown(input.SPACE)) {
    Bullet b = new Bullet(you.getX(), you.getY(), you.getRotation());
    world.add(b);
  }

  cam.setDestination(you.getX(), you.getY());
  cam.update();
  
  background(255);
  
  pushMatrix();
  translate((displayWidth/2.0) - cam.getX(), (displayHeight/2.0) - cam.getY());
  world.draw();
  popMatrix();
  world.step();
  blink.draw();
}

void keyPressed() {
  input.keyPressed();
}

void keyReleased() {
  input.keyReleased();
}

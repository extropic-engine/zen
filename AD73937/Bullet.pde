// TODO:
// Bullets leak memory right now (they never go away even offscreen)
// Bullets appear in the center of the shooting entity rather than in front.
class Bullet extends FCircle {
  
  final float BULLET_SPEED = 1000;
  
  Bullet(float x, float y, float rotation) {
    super(10);
    this.setPosition(x, y);
    
    PVector orientation = PVector.mult(PVector.fromAngle(rotation), BULLET_SPEED);
    this.setVelocity(orientation.x, orientation.y);
    this.setGroupIndex(-1);
    this.setBullet(true);
  }
}

import UnityEngine

class PlayerMove (MonoBehaviour): 

	speed = 5;

	def Start ():
		pass
	
	def Update ():
		if Input.GetKey(KeyCode.UpArrow):
			transform.Translate(Vector2(0,1) * Time.deltaTime * speed);
		elif Input.GetKey(KeyCode.DownArrow):
			transform.Translate(Vector2(0,-1) * Time.deltaTime * speed);
		elif Input.GetKey(KeyCode.LeftArrow):
			transform.Translate(Vector2(-1,0) * Time.deltaTime * speed);
		elif Input.GetKey(KeyCode.RightArrow):
			transform.Translate(Vector2(1,0) * Time.deltaTime * speed);

class MeepMouse extends Meep {
  // TODO: this is not overwriting super class variables
  final float friction = 0.001;
  
  boolean goLeft = false;
  
  MeepMouse(String imageFile) {
    super(imageFile);
  }
  
  void update() {
    if (goLeft) {
      runLeft();
    } else {
      runRight();
    }
    super.update();
  }
  
  void slamIntoWall() {
    goLeft = !goLeft;
    super.slamIntoWall();
  }
}

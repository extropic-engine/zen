import ddf.minim.*;
import ddf.minim.signals.*;
import ddf.minim.analysis.*;
import ddf.minim.effects.*;

// Things needed:
// Meep: a character or enemy in the game
// Map: stores level images and collision info
// 

Meep sheep;
ArrayList<Meep> meeps;
Planet meepleton;
Oculus ocular;

void setup() {
  size(screen.width, screen.height);
  background(128,128,128);
  smooth();
  frameRate(60);

  meeps = new ArrayList<Meep>();
  sheep = new Meep("sheep.png");
  Meep mouse = new MeepMouse("sheep.png");
  meeps.add(sheep);
  meeps.add(mouse);
  meepleton = new Planet();
  ocular = new Oculus();
  sheep.location.x = 64;
  /*minim = new Minim(this);
  player = minim.loadFile("song.mp3");
  player.play();*/
}

void draw() {
  
  // process player actions
  if (l_key) {
    sheep.runLeft();
  }
  if (r_key) {
    sheep.runRight();
  }
  
  
  // do the updates first
  for (Meep m : meeps) {
    m.update();
  }
  background(128);
  meepleton.draw();
  // now check for meep collisions and assign actions
  for (Meep m : meeps) {
    // iterate across all spanned tiles
    for (int r = (int) m.location.x / tileSize; r < (int) (m.location.x + m.meepSize - 1) / tileSize + 1; r++) {
      for (int c = (int) m.location.y / tileSize; c < (int) (m.location.y + m.meepSize - 1) / tileSize + 1; c++) {
        if (r < 0 || c < 0 || r >= meepleton.planetWidth) {
          // meep is out of bounds!
          continue;
        }
        if (c >= meepleton.planetHeight) {
          m.die();
          break;
        }
        if (meepleton.typeTiles[r][c] == Planet.KILL) {
          m.die();
          break;
        }
        
        if (meepleton.typeTiles[r][c] == Planet.COLLIDE) {
          // if we fall onto the floor
          // bug with this code: if the fall velocity is greater than meepSize * 0.5, the meep will shoot straight through the floor
          if (c+1 >= (int) (m.location.y + m.meepSize - 1) / tileSize + 1 && abs(c * tileSize - (m.location.y + m.meepSize)) < m.meepSize * 0.5) {
            m.location.y = (c * tileSize) - m.meepSize;
            m.hitFloor();

          }
          // if moving left and collision detected on left side
          else if (m.velocity.x < 0 && r == (int) m.location.x / tileSize) {
            m.location.x = (r+1) * tileSize;
            m.slamIntoWall();
          }
          
          // if moving right and collision detected on right side
          else if (m.velocity.x > 0 && r+1 >= (int) (m.location.x + m.meepSize - 1) / tileSize + 1) {
            m.location.x = (r * tileSize) - m.meepSize;
            m.slamIntoWall();
          }
          
          // if we bonk into a ceiling
          if (c == (int) m.location.y / tileSize) {
            m.velocity.y = 0;
          }
        }
        
        // debug: show spannned tiles;
        //stroke(255, 0, 0);
        //rect(r * tileSize, c * tileSize, tileSize, tileSize);
      }
    }
  }
  //ocular.centerOn(sheep.location.x + (meepSize/2), sheep.location.y + (meepSize/2));
  

  
  for (Meep m : meeps) {
    m.draw();
  }
}

// key pressed state
boolean l_key = false;
boolean r_key = false;

void keyPressed() {
  if (key == ' ') {
    sheep.jump();
  } else if (key == 'q') {
    exit();
  } else if (key == CODED) {
    if (keyCode == LEFT) {
      l_key = true;
    } else if (keyCode == RIGHT) {
      r_key = true;
    }
  }
}

void keyReleased() {
  if (key == CODED) {
    if (keyCode == LEFT) {
      l_key = false;
    } else if (keyCode == RIGHT) {
      r_key = false;
    }
  }
}

void stop() {
  /*player.close();
  minim.stop();
  super.stop();*/
}

// the dimensions of a square tile
final int tileSize = 32;

// A Planet is a place where many interesting things might happen.
class Planet {
  // tile types
  final static int NOTHING = 0; // open air
  final static int COLLIDE = 1; // walls
  final static int KILL = 2;    // lava, spikes, etc
    
  int planetWidth;
  int planetHeight;
  
  PImage images[];
  
  int imageTiles[][];
  int typeTiles[][];
  
  Planet() {
    planetWidth = 120;
    planetHeight = 120;
    imageTiles = new int[planetWidth][planetHeight];
    typeTiles = new int[planetWidth][planetHeight];
    images = new PImage[2];
    images[0] = loadImage("sky.png");
    images[1] = loadImage("grass.png");
    for (int i = 0; i < 15; i++) {
      imageTiles[i][10] = 1;
      typeTiles[i][10] = 1;
    }
    imageTiles[0][9] = 1;
    typeTiles[0][9] = 1;
    imageTiles[14][9] = 1;
    typeTiles[14][9] = 1;
  }
  
  void draw() {
    for (int r = 0; r < width / tileSize; r++) {
      for (int c = 0; c < height / tileSize; c++) {
        image(images[imageTiles[r][c]], ocular.location.x + (r*tileSize), ocular.location.y + (c*tileSize));
      }
    }
  }
}

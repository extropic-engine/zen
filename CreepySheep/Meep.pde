// A meep is any creature that runs about.
class Meep {
  // meep constants... these can vary by different kinds of meep but right now they are hardcoded
  final float runAcceleration = 2.0;
  final float jumpAcceleration = 15.0;
  final float friction = 0.8; // right now this is just constant but it could vary by platform type... to enable icy platforms and such
  final float gravity = 1.0;
  final float maxVelocity = 30.0;
  final int meepSize = 64; // the size of the meep image and hitbox
  
  PImage pic;
  PVector location;
  PVector velocity;
  boolean dead = false;
  int type;
  
  Meep(String imageFile) {
    location = new PVector();
    velocity = new PVector();
    pic = loadImage(imageFile);
  }
  
  void update() {
    // apply friction
    velocity.x *= friction;
    
    // apply gravity
    velocity.y += gravity;
    if (velocity.y > maxVelocity) {
      velocity.y = maxVelocity;
    }
    
    // apply speed of light
    if (velocity.x > maxVelocity) {
      velocity.x = maxVelocity;
    } else if (velocity.x < -maxVelocity) {
      velocity.x = -maxVelocity;
    }
    if (velocity.y > maxVelocity) {
      velocity.y = maxVelocity;
    } else if (velocity.y < -maxVelocity) {
      velocity.y = -maxVelocity;
    }
    
    // update location
    location.x += velocity.x;
    location.y += velocity.y;
  }
  
  void draw() {
    image(pic, ocular.location.x + location.x, ocular.location.y + location.y);
  }
  
  void die() {
    // transition into "noclip" mode and "bounce" to your death
    dead = true;
    bounce();
  }
  
  void jump() {
    // if standing on something, bounce
    // else, do nothing
    // for now just bounce unconditionally
    bounce();
  }
  
  // a bounce is an unconditional jump
  // triggers on killing enemies or double-jumping
  void bounce() {
    velocity.y -= jumpAcceleration;
  }
  
  void slamIntoWall() {
    // could also do a cute slam animation here, once animations are implemented
    velocity.x = (-velocity.x * 1.5);
    if (abs(velocity.x) < runAcceleration * 3) {
      velocity.x = 0;
    }
  }
  
  void hitFloor() {
    velocity.y = (-velocity.y * 0.2);
    if (abs(velocity.y) < 1) {
      velocity.y = 0;
    }
  }
  
  void runLeft() {
    if (velocity.x > 0) {
      // TODO: skid when changing direction suddenly
    }
    velocity.x -= runAcceleration;
  }
  
  void runRight() {
    if (velocity.x < 0) {
      //TODO: skid when changing direction suddenly
    }
    velocity.x += runAcceleration;
  }
}

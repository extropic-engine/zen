// Oculus watches over the Planet Meepleton.
// This is the camera class that controls the viewport area.
class Oculus {
  PVector location;
  
  Oculus() {
    location = new PVector(0,0);
  }
  
  void centerOn(float x, float y) {
    location.x = (width/2) - x;
    location.y = (height/2) - y;
    // TODO: fix camera going off the edge of the screen
    /*if (location.x < 0) {
      //location.x = 0;
    }
    if (location.y < 0) {
      //location.y = 0;
    }*/
  }
}

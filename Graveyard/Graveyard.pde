// This is a collection of classes that ended up never being used in their
// respective programs, but the code was potentially interesting enough to
// be of future use. They all need to be cleaned up and repackaged, but it
// is the concepts that are of value.

class Monitor {
  PFont font;
  Object o;
  
  Monitor(Object o) {
    font = createFont("Serif", 24);
    textFont(font);
    this.o = o;
  }
  
  void draw() {
    stroke(0);
    rect(0,0,100,100);
    fill(0);
    text("Value: " + o.toString(),0,0,100,100);
    noFill();
  }
}

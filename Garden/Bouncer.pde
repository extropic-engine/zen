class Bouncer {
  PVector location;
  PVector velocity;
  float x;
  float y;
  float radius;
  float xVel;
  float yVel;
  
  void setup() {
    location = new PVector(100,100);
    velocity = new PVector(2.5, 5);
  }
  
  void draw() {
    location.add(velocity);
    if (((location.x + radius) > width) || ((location.x - radius) < 0)) {
      velocity.x = velocity.x * -1;
    }
    if (((location.y + radius) > height) || ((location.y - radius) < 0)) {
      velocity.y = velocity.y * -1;
    }
  }
}

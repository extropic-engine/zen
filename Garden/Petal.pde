class Petal {
  int x = 100;
  int y = 100;
  
  void draw() {
    beginShape();
    vertex(x, y);
    bezierVertex(x + 150, y - 10, x + 400, y + 100, x + 400, y + 100);
    bezierVertex(x + 300, y, x + 400,  y - 100, x + 400, y - 100);
    bezierVertex(x + 150, y - 10, x, y, x,      y);
    endShape();
  }
}

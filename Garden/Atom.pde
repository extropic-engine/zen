class Atom extends Bouncer {
  ArrayList<Orbiter> orbitals = new ArrayList<Orbiter>();
  ColorBreather breath;
  
  Atom(float newX, float newY) {
    location = new PVector(newX, newY);
    velocity = new PVector(random(-2,2), random(-2,2));

    breath = new ColorBreather();
    radius = int(random(10,100));
    if (newX - radius < 0) {
      location.x += radius;
    } else if (newX + radius > width) {
      location.x -= radius;
    }
    if (newY - radius < 0) {
      location.y += radius;
    } else if (newY + radius > height) {
      location.y -= radius;
    }
    
    int electrons = int(random(2,10));
    for (int i = 0; i < electrons; i++) {
      orbitals.add(new Orbiter(newX, newY, radius));
    }
  }
  
  void draw() {
    super.draw();
    breath.draw();
    stroke(breath.currentColor);
    for (Orbiter o : orbitals) {
      o.x = location.x;
      o.y = location.y;
      o.draw();
    }
  }
}

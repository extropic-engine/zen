class Orbiter {
  float spin = random(0.01, 0.1);
  float rotation = random(PI);
  float x;
  float y;
  float w;
  float h;
  
  Orbiter(float newX, float newY, float radius) {
    x = newX;
    y = newY;
    w = (radius * 2);
    h = radius;
  }
  
  void draw() {
    rotation += spin;
    
    pushMatrix();
    translate(x, y);
    rotate(rotation);
    ellipse(0,0,w,h);
    popMatrix();
  }
}

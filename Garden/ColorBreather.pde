class ColorBreather {
  color currentColor;
  float r = random(0,255);
  float g = random(0,255);
  float b = random(0,255);
  float a = random(128,255);
  float rDelta = random(-2,2);
  float gDelta = random(-2,2);
  float bDelta = random(-2,2);
  float aDelta = 0;
  float rLow = 0;
  float gLow = 0;
  float bLow = 0;
  float aLow = 0;
  float rHigh = 255;
  float gHigh = 255;
  float bHigh = 255;
  float aHigh = 0;
  
  void draw() {
    r += rDelta;
    g += gDelta;
    b += bDelta;
    a += aDelta;
    if (r <= rLow || r >= rHigh) { rDelta = -rDelta; }
    if (g <= gLow || g >= gHigh) { gDelta = -gDelta; }
    if (b <= bLow || b >= bHigh) { bDelta = -bDelta; }
    if (a <= bLow || a >= bHigh) { aDelta = -aDelta; }
    
    currentColor = color(r,g,b,a);
  }
  
  void pink() {
    r = 300;
    rLow = 290;
    gLow = 20;
    bLow = 20;
    rHigh = 310;
    gHigh = 90;
    bHigh = 90;
  }
}

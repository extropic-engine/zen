class Flower extends Bouncer {
  ArrayList<Orbiter> orbitals = new ArrayList<Orbiter>();
  ColorBreather breath;
  
  Flower(float newX, float newY) {
    location = new PVector(newX, newY);
    velocity = new PVector(0, 0);    // flowers are stationary

    breath = new ColorBreather();
    radius = random(25,100);
    int electrons = int(random(2,10));
    float spin = random(0.01, 0.1);
    for (int i = 0; i < electrons; i++) {
      Orbiter o = new Orbiter(newX, newY, radius);
      o.spin = spin;
      o.rotation = (PI / electrons) * i;
      orbitals.add(o);
    }
  }
  
  void draw() {
    super.draw();
    breath.draw();
    fill(breath.currentColor);
    stroke(breath.currentColor);
    for (Orbiter o : orbitals) {
      o.x = location.x;
      o.y = location.y;
      o.draw();
    }
  }
}


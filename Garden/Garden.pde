//import ddf.minim.*;
//import ddf.minim.signals.*;
//import ddf.minim.analysis.*;
//import ddf.minim.effects.*;

/*
Ideas:
Set up defined ranges for color breathers
Add petal object that drifts from top to bottom
Do a verification step so flowers are not layered on top of each other.
*/

ArrayList<Atom> atoms = new ArrayList<Atom>();
ArrayList<Flower> flowers = new ArrayList<Flower>();
ColorBreather bgBreather = new ColorBreather();
//Minim minim;
//AudioPlayer player;
//Petal petal = new Petal();

void setup() {
  size(screen.width, screen.height);
  background(128,128,128);
  smooth();
  frameRate(60);
  for (int i = 0; i < 10; i++) {
    atoms.add(new Atom(random(0, width), random(0, height)));
  }
  for (int i = 0; i < 10; i++) {
    flowers.add(new Flower(random(0, width), random(0, height)));
  }
  //minim = new Minim(this);
  //player = minim.loadFile("song.mp3");
  //player.play();

  colorMode(HSB, 360, 100, 100);
  Flower f = new Flower(width/2, height/2);
  f.breath.pink();
  flowers.add(f);
}


void draw() {
  bgBreather.draw();
  background(bgBreather.currentColor);
  for (Atom a : atoms) {
    a.draw();
  }
  for (Flower f : flowers) {
    f.draw();
  }
  //petal.draw();
  noFill();
}

void stop() {
  //player.close();
  //minim.stop();
  super.stop();
}

**A series of short sketches in screensaver style. All code in this repository licensed under the WTFPL. See the file COPYING for license terms.**

Garden
======

A sketch with pulsating colors, bouncing atoms, and spinning flowers. Written for my sister's 21st birthday.

Trace
======

A sketch with pulsating boxes that trace paths across the screen as they navigate around each other. Written for a friend's birthday.

Gravity
======

An interactive sketch that simulates gravity in a Newtonian manner. Aesthetic concerns were higher than scientific accuracy. There are many configuration options for customizing the appearance of the particles.
